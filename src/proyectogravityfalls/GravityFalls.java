/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectogravityfalls;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Carolina Burgos, Luis Moya, Eduardo Salazar
 */
public class GravityFalls {
    
    public static void main(String[] args) {
        
        System.out.println("\033[31m*************************** CAESAR CIPHER ***************************\n");
        
        String[] cTextListCaesar1 = {"VWDQ LV QRW ZKDW KH VHHPV",
                            "ZHOFRPH WR JUDYLWB IDOOV.",
                            "QHAW ZHHN: UHWXUQ WR EXWW LVODQG.",
                            "KH'V VWLOO LQ WKH YHQWV.",
                            "FDUOD, ZKB ZRQ'W BRX FDOO PH?",
                            "RQZDUGV DRVKLPD!",
                            "PU. FDHVDULDQ ZLOO EH RXW QHAW ZHHN. PU. DWEDVK ZLOO VXEVWLWXWH.",
                            "SXEHUWB LV WKH JUHDWHVW PBVWHUB RI DOO DOVR: JR RXWVLGH DQG PDNH IULHQGV.",
                            "OLHV",
                            "PBVWHUB VKDFN",
                            "SLWW",
                            "ELOO LV ZDWFKLQJ",
                            "IURP WKH ILUVW XQWLO WKH ODVW VHDUFK WKH",
                            "WKHP DOO ZHOFRPH WR JUDYLWB IDOOV",
                            "FRGHV RI FUHGLWV SDVW RQH PHDQV RQH VR VHDUFK",
                            "ZDWFK RXW",
                            "NLOO PH SOHDVH",
                            "ZLGGOH",
                            "VKLIWHU",
                            "ZKDWHYV",
                            "EHDUR",
                            "VWDQ LV QRW ZKDW KH VHHPV",
                            "JXYDPHQW"
                        };
        
        String[] cTextListCaesar2 = {"OAUVG",
                            "EWTUG AQW OCTKNAP"};
        
        String[] cTextListCaesar3 = {"TEV FP TBKAV PL MBOCBZQ"};
        
        for (String cText: cTextListCaesar1) {            
            System.out.println("Cryptogram(s): " + cText);
            System.out.println("Decoded: " + caesarDecipher(cText) + "\n");
        }

        for (String cText: cTextListCaesar2) {
            System.out.println("Cryptogram(s): " + cText);
            System.out.println("Decoded: " + caesarDecipherLeft(cText, 2) + "\n");
        }
        
        for (String cText: cTextListCaesar3) {
            System.out.println("Cryptogram(s): " + cText);
            System.out.println("Decoded: " + caesarDecipherRight(cText, 3) + "\n");
        }
        
        System.out.println("\n\033[31m*************************** ATBASH CIPHER ***************************\n");
        String[] cTextListAtbash = {"KZKVI QZN WRKKVI HZBH: \"ZFFTSDCJSTZWHZWFS!\"", 
                        "V. KOFIRYFH GIVNYOVB.",
                        "MLG S.T. DVOOH ZKKILEVW.",
                        "HLIIB, WRKKVI, YFG BLFI DVMWB RH RM ZMLGSVI XZHGOV.",
                        "GSV RMERHRYOV DRAZIW RH DZGXSRMT.",
                        "YILFTSG GL BLF YB SLNVDLIP: GSV XZMWB.",
                        "SVZEB RH GSV SVZW GSZG DVZIH GSV UVA.",
                        "KFIV VMVITB, MLG HPRM ZMW YLMV",
                        "IRHRMT ORPV GSV HSVKZIW GLMV",
                        "YROO XRKSVI! GIRZMTOV!",
                        "YROO XRKSVI GIRZMTOV"
                        };
        
        for (String cText: cTextListAtbash) {
            System.out.println("Cryptogram(s): " + cText);
            System.out.println("Decoded: " + atbashCipher(cText) + "\n");
        }

        System.out.println("\n\033[31m*************************** COMBINED CIPHER ***************************\n");
        String[] cTextListCombined = {"5-19-23-6-21-16 18-9-6 4-16-19 22-12-15-10-20-19-25-19",
                        "5-19-23-6-21-16 18-9-6 4-16-19...",
                        "4-16-19 11-23-10 20-9-1-10-5-4-23-15-6-5 15-5 2-19-6-25 21-12-19-2-19-6",
                        "21-23-10 16-19 16-15-20-19 16-15-5 8-12-23-10-5 18-9-6-19-2-19-6?",
                        "15-11-8-6-9-8-19-6 3-5-19 9-18 11-23-21-16-15-10-19-6-25 21-9-3-12-20",
                        "12-19-23-20 4-9 3-4-4-19-6 21-23-4-23-5-4-6-9-8-16-19",
                        "9-12-20 11-23-10 5-12-19-19-8-15-10-17 9-10 4-16-19 17-6-19-19-10",
                        "21-23-10'4 16-19-12-8 22-3-4 1-9-10-20-19-6 1-16-23-4 16-19'5 5-19-19-10",
                        "10-9 8-3-8-8-19-4 5-4-6-15-10-17-5 21-23-10 16-9-12-20 11-19 20-9-1-10",
                        "5-9 8-23-4-15-19-10-4-12-25 15 1-23-4-21-16 4-16-15-5 4-9-1-10",
                        "23-22-10-9-6-11-23-12 5-9-9-10 1-15-12-12 22-19 4-16-19 10-9-6-11",
                        "19-10-14-9-25 4-16-19 21-23-12-11 22-19-18-9-6-19 4-16-19 5-4-9-6-11",
                        "1-15-10-10-15-10-17 16-19-23-6-4-5 22-25 20-23-25-12-15-17-16-4",
                        "8-9-5-5-19-5-5-15-10-17 6-9-22-9-4-5 22-25 11-9-9-10-12-15-17-16-4",
                        "16-19-6 19-11-9-4-15-9-10-23-12 22-23-17-17-23-17-19 15-5 23 6-19-23-12 18-6-15-17-16-4",
                        "5-16-19 16-23-5 4-16-19 9-10-19 10-23-11-19 17-15-18-18-23-10-25",
                        "23-12-12 23-10-15-11-23-4-15-9-10",
                        "15-5 22-12-23-21-13 11-23-17-15-21",
                        "17-15-20-19-9-10’5 4-23-10-4-6-3-11-5, 11-15-5-5-8-19-12-12-19-20 4-23-4-4-9-9-5,",
                        "5-16-23-10-20-6-23’5 6-19-14-19-21-4-15-9-10-5, 5-9-21-15-19-4-25’5 2-15-19-1-5,",
                        "23 18-19-23-6 9-18 1-15-4-21-16-19-5, 23 12-15-18-19 9-18 6-19-17-6-19-4,",
                        "4-16-19-5-19 23-6-19 4-16-19 4-16-15-10-17-5 4-16-23-4 4-16-19-25 4-6-25 4-9 18-9-6-17-19-4",
                        "14-9-15-10 4-16-19 4-15-11-19 8-23-6-23-20-9-26 23-2-9-15-20-23-10-21-19 19-10-18-9-6-21-19-11-19-10-4 5-7-3-23-20-6-9-10!",
                        "17-6-19-23-4 16-9-3-6-5!",
                        "5-9-12-15-20 22-19-10-19-18-15-4-5!",
                        "5-15-17-10 3-8 25-19-5-4-19-6-20-23-25!",
                        "23-4 4-16-19 8-12-23-25 9-6 23-4 4-16-19 18-23-15-6,",
                        "15 23-12-1-23-25-5 5-19-19 4-16-19-11 5-4-23-10-20-15-10-17 4-16-19-6-19",
                        "20-6-19-5-5-19-20 15-10 22-12-23-21-13 4-16-19-25'6-19 9-10 11-25 12-23-1-10,",
                        "22-3-4 1-16-19-10 15 4-3-6-10 11-25 16-19-23-20 4-16-19-25'6-19 17-9-10-19",
                        "5-4-23-10-15-5-10-9-4-1-16-23-4-16-19-5-19-19-11-5",
                        "4-16-15-6-4-25 25-19-23-6-5 23-10-20 10-9-1 16-19’5 22-23-21-13",
                        "4-16-19 11-25-5-4-19-6-25 15-10 4-16-19 11-25-5-4-19-6-25 5-16-23-21-13",
                        "23 5-4-3-22-22-9-6-10 4-9-3-17-16 10-19-1 14-19-6-5-19-25 10-23-4-15-2-19",
                        "18-15-12-22-6-15-21-13 1-23-5-10'4 4-9-9 21-6-19-23-4-15-2-19",
                        "16-23-2-15-10-17 4-1-15-10-5 1-23-5 10-9-4 16-15-5 8-12-23-10",
                        "5-9 16-19 14-3-5-4 5-16-6-3-17-17-19-20 23-10-20 10-23-11-19-20 22-9-4-16 5-4-23-10",
                        "18-3-10 23-10-20 17-23-11-19-5 23-6-19 17-6-19-23-4 20-15-5-4-6-23-21-4-15-9-10-5",
                        "22-3-4 5-11-23-12-12 4-16-15-10-17-5 21-23-10 16-23-2-19 21-16-23-15-10 6-19-23-21-4-15-9-10-5",
                        "22-19 1-23-6-25 9-18 1-16-9-11 25-9-3 22-19-12-15-4-4-12-19",
                        "22-15-17 8-6-9-22-12-19-11-5 21-23-10 5-4-23-6-4 9-3-4 1-15-20-20-12-19",
                        "15-10 21-15-8-16-19-6'5 17-23-11-19 16-19 10-19-19-20-5 23 8-23-1-10",
                        "22-19 5-3-6-19 4-9 13-10-9-1 1-16-15-21-16 5-15-20-19 25-9-3'6-19 9-10",
                        "21-23-6-12-23 11-21-21-9-6-13-12-19 6-19-4-3-6-10-19-20 23-12-12 16-15-5 18-12-9-1-19-6-5",
                        "11-23-6-15-12-25-10 20-15-2-9-6-21-19-20 16-15-11 23-18-4-19-6 9-10-12-25 5-15-26 16-9-3-6-5",
                        "22-19-23-4-6-15-21-19 5-12-23-8-8-19-20 16-15-11 18-9-6 22-19-15-10-17 23 21-23-20",
                        "9-12-20 17-9-12-20-15-19'5 4-16-19 22-19-5-4 17-15-6-12-18-6-15-19-10-20 5-4-23-10 19-2-19-6 16-23-20",
                        "4-16-19 8-6-9-8-16-19-21-25 5-19-19-11-19-20 18-23-6 23-1-23-25",
                        "22-3-4 18-15-10-23-12-12-25 1-19'2-19 6-19-23-21-16-19-20 4-16-19 20-23-25.",
                        "17-15-2-19 3-8 4-16-19 8-23-5-4. 19-11-22-6-23-21-19 4-16-19 5-4-6-23-10-17-19.",
                        "19-2-19-6-25-4-16-15-10-17 25-9-3 21-23-6-19 23-22-9-3-4 1-15-12-12 21-16-23-10-17-19.",
                        "17-23-11-19 15-5 9-2-19-6, 23-10-20 15 1-9-10",
                        "10-9-1 15-4'5 4-15-11-19 4-9 5-4-23-6-4 4-16-19 18-3-10",
                        "15 23-12-1-23-25-5 12-9-2-19 21-9-6-6-3-8-4-15-10-17 12-15-2-19-5",
                        "10-9-1 12-19-4'5 5-19-19 1-16-15-21-16 8-15-10-19-5 5-3-6-2-15-2-19-5",
                        "1-16-19-10 9-10-19 17-19-4-5 4-6-23-8-8-19-20 15-10-5-15-20-19 4-16-19 8-23-5-4",
                        "20-6-19-23-11-5 21-23-10 4-3-6-10 4-9 10-15-17-16-4-11-23-6-19-5 18-23-5-4",
                        "4-19-10 5-25-11-22-9- 5 8-12-23-21-19-20 23-6-9-3-10-20 23 1-16-19-19-12",
                        "16-23-10-20 15-10 16-23-10-20 4-16-19-25'12-12 22-9-10-20 4-16-19 5-19-23-12",
                        "22-3-4 22-6-19-23-13 4-16-19 21-16-23-15-10, 23-10-20 8-23-25 4-16-19 21-9-5-4",
                        "4-16-19 8-6-9-8-16-19-21-25 1-15-12-12 23-12-12 22-19 12-9-5-4",
                        "18-23-20-19-20 8-15-21-4-3-6-19-5 22-12-19-23-21-16-19-20 22-25 5-3-10",
                        "4-16-19 4-23-12-19'5 4-9-12-20, 4-16-19 5-3-11-11-19-6'5 20-9-10-19",
                        "15-10 11-19-11-9-6-15-19-5 4-16-19 8-15-10-19-5 5-4-15-12-12 8-12-23-25",
                        "9-10 23 5-3-10-10-25 5-3-11-11-19-6'5 20-23-25"
                        };
               
        for (String cText: cTextListCombined) {
            System.out.println("Cryptogram(s): " + cText);
            System.out.println("Decoded: " + combinedCipher(cText) + "\n");
        }
        
        System.out.println("\n\033[31m*************************** A1Z26 CIPHER ***************************\n");
        String[] cTextListA1Z26 = {"14-5-24-20 21-16: \"6-15-15-20-2-15-20 20-23-15: 7-18-21-14-11-12-5'19 7-18-5-22-5-14-7-5.\"", 
                        "22-9-22-1-14 12-15-19 16-1-20-15-19 4-5 12-1 16-9-19-3-9-14-1.",
                        "2-21-20 23-8-15 19-20-15-12-5 20-8-5 3-1-16-5-18-19?",
                        "8-1-16-16-25 14-15-23, 1-18-9-5-12?",
                        "9-20 23-15-18-11-19 6-15-18 16-9-9-9-9-9-9-9-9-9-9-9-9-9-9-9-9-9-7-19!",
                        "20-15 2-5 3-15-14-20-9-14-21-5-4...",
                        "18-5-22-5-18-19-5 20-8-5 3-9-16-8-5-18-19",
                        "6-12-1-7"
                        };
        
        for (String cText: cTextListA1Z26) {
            System.out.println("Cryptogram(s): " + cText);
            System.out.println("Decoded: " + A1Z26(cText) + "\n");
        }        
        
        System.out.println("\n\033[31m*************************** BINARY CIPHER ***************************\n");
        String[] cTextListBinary = {"010100000111010101110\n" +
                            "1000010000001100001\n" +
                            "011011000110110000100\n" +
                            "000011100110110100101\n" +
                            "11100000100000011100\n" +
                            "000110100101100101011\n" +
                            "000110110010101110011 \n" +
                            "00100000011101000110\n" +
                            "11110110011101100101011\n" +
                            "10100011010000110010\n" +
                            "10111001000100001",
                            "01010011010100000100000101000011010001010100\n" +
                            "10100100000101001101010101000101011101001111"
                        };
        
        for (String cText: cTextListBinary) {
            System.out.println("Cryptogram(s): \n" + cText);
            System.out.println("Decoded: " + binary(cText) + "\n");
        }
        
        System.out.println("\n\033[31m**************************** VIGENERE CIPHER ****************************\n");
        String[] claves = {"WIDDLE",
                            "SHIFTER",
                            "WHATEVS",
                            "CIPHER",
                            "BEARO",
                            "NONCANON",
                            "ERASE",
                            "CAPACITOR",
                            "GOATANDAPIG",
                            "CURSED",
                            "STNLYMBL",
                            "SIXER",
                            "RADMASTER",
                            "WORKINIT",
                            "SCHMENDRICK",
                            "DOPPER",
                            "BLUEBOOK",
                            "CILLBIPHER",
                            "DIPPYFRESH",
                            "SHACKTRON",
                            "HIDDENDEEPWITHINTHEWOODSABURIEDTREASUREWAITS",
                            "AXOLOTL"
                        };
        
        String[] cTextListVigenere = {"SMOFZQA JDFV",
                            "OOIY DMEV VN IBWRKAMW BRUWLL",
                            "NLMXQWWN IIZ LZFNF",
                            "YM’KL ECN PPK WFOM UBR KQVXNLK, DCI SIK’U VDA JFTOTA AYQ BWL VVCT \"EBTGGB BHWKGZH\" HVV: TMEASZFA LOS YCDT PRWKTIYEKGL DBV XQDTYRDGVI",
                            "BRTYMEMNX QBR HRRQPEE",
                            "PVREK BIG QF. JCDQZRF’ ZNVEFH OBCX: \"C BEWRS VVUTBFL BT BKNX CVAY BKNX CVAY BKNX\"",
                            "MXNGVEECW MW SLAWW. SUL FPZSK MW SOJMRX.",
                            "FOC'T FW MVV VIBE EZBAV KF NOW KTB'K FO IHG BBAV VIBE.",
                            "O SAM KVGS.",
                            "PYOL YS QH LLFDJW: UAH DNCVFW ZTCKW XKG WFFWWKNLLMRP? WISAGCXJ AR WKUISW! DPX WDSUKXR: LLH UBFO.",
                            "LAR ZPUHTFTY XWEUPJR GHGZT",
                            "TIZOLHAJSIW CKMMWZPMKQ: GLY KJQBH",
                            "VXFQLKB-AYRTHHEJ!",
                            "CWZSQVQBEWZSQVQBEWZSQVQMPHKD 'MZ!",
                            "S UPYTYH DIP GAVO QETHI MCBK OHK XEXJB VRW YOUWCHIA VRSV OQ LRDIA",
                            "VCDH, PZNS P CSSOS VDPUHB GTXILSKTV, VYSCIYROZN USLQR WXW NDM WDQVZOGS, EEG PTUVZHBSTH R WOAZMEJ PJAPURU PCH JDGHN GRW OADRX WVT LEP",
                            "ETX CPI ASTD GI?",
                            "KB HTMT IHOV 1,000 AMLCT NDY XZOM MLCG'H TSCGKFWFA IV VVEWYDUQIBXV, CVO HIMC OI'J DINV, IM'H NSZPO EZ CM KLVP EZLYLG",
                            "FZPO YSU BQSHZ LTLY FR LV UCC IFJ CIYHO LTEYWKQWUW II P KFASJ JKQASPJE'W LLOMKXQNFR FLWEDGI",
                            "KVOU VTKSE XVREOW DQTMJKGD MF KNLJH CVE 900 YCHJZ OH XXFB PJPSKC FVQUSIOV LHP: FRNLLCDBFBF",
                            "ZMFUIGV PSHP IGK AGTAYAG TRMNE VVGSQW KLE JOJXU GIMWZ",
                            "GLCOPRP GOOGWMJ FXZWG"
                        };
        
        for (int i = 0; i < cTextListVigenere.length; i++) {
            System.out.println("Cryptogram(s): " + cTextListVigenere[i]);
            System.out.println("Decoded: " + vigenere(cTextListVigenere[i],claves[i]) + "\n");
        }        
        
        System.out.println("\n\033[31m***************************** SHORTS SECTION *****************************\n");
        System.out.println("Caesar Cipher\n");
        System.out.println("Episode\t\t\tDecoded\n");
        System.out.println("Candy Monster\t\t" + caesarDecipher("IURP WKH ILUVW XQWLO WKH ODVW VHDUFK WKH")); 
        System.out.println("Lefty\t\t\t" + caesarDecipher("WKHP DOO ZHOFRPH WR JUDYLWB IDOOV"));
        System.out.println("Tooth\t\t\t" + caesarDecipher("FRGHV RI FUHGLWV SDVW RQH PHDQV RQH VR VHDUFK"));
        
        System.out.println("\n\nNumeric Codes\n");
        System.out.println("Episode\t\t\tDecoded\n");
        System.out.println("Candy Monster\t\t" + shortsNumericCode("[13)8,9,10][14,17,22 "));
        System.out.println("Stan's Tattoo\t\t" + shortsNumericCode("[1)14][2)5,24 3)3][5)7,9]"));
        System.out.println("Mailbox\t\t\t" + shortsNumericCode("[6)33,40,46 9)1,18][10)32,33][39 "));
        System.out.println("Lefty\t\t\t" + shortsNumericCode("[17)6,12 20)3,4]"));
        System.out.println("Tooth\t\t\t" + shortsNumericCode("[14)21,30,32 15)13,20][22 16)20"));
        System.out.println("The Hide-Behind\t\t" + shortsNumericCode("11)4,12,18][12)8,9][17,18]"));
        
    }
    
    /*Caesar Cipher*/
    public static String caesarDecipherLeft(String cText, Integer desp) {
        String[] CRYPTOGRAM = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
        
        String decoded = "";
        for(int i=0;i<cText.length();i++){
            String letra = String.valueOf(cText.charAt(i));
            String temp = "";
            for(int j=0; j < CRYPTOGRAM.length; j++){
                if(letra.equalsIgnoreCase(CRYPTOGRAM[j])){
                    int pos = j - desp;
                    if(pos < 0){
                        temp += CRYPTOGRAM[26+pos];
                    } else {
                        temp += CRYPTOGRAM[pos];
                    }
                    break;
                } 
            }
            if(temp.equals("")){
                decoded += letra;
            } else {
                decoded += temp;
            }
        }
        return decoded;
    }
    
    public static String caesarDecipherRight(String cText, Integer desp) {
        
        String[] CRYPTOGRAM = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
        
        String decoded = "";
        for(int i=0;i<cText.length();i++){
            String letra = String.valueOf(cText.charAt(i));
            String temp = "";
            for(int j=0; j < CRYPTOGRAM.length; j++){
                if(letra.equalsIgnoreCase(CRYPTOGRAM[j])){
                    int pos = j + desp;
                    if(pos > 25){
                        temp += CRYPTOGRAM[pos-26];
                    } else {
                        temp += CRYPTOGRAM[pos];
                    }
                    break;
                } 
            }
            if(temp.equals("")){
                decoded += letra;
            } else {
                decoded += temp;
            }
        }
        return decoded;
    }
    
    public static String caesarDecipher(String cText) {
        return caesarDecipherLeft(cText, 3);
    }
    
    /*Atbash Cipher*/
    public static String atbashCipher(String cText) {
        String[] ALPHABET = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
        HashMap<String, String> CRYPTOGRAM = new HashMap<String,String>();
        
        int tam = ALPHABET.length;
        for (int i=0; i < tam; i++){
            CRYPTOGRAM.put(ALPHABET[i], ALPHABET[tam-i-1]);
        }
        
         String dText = "";
        for (int i = 0; i < cText.length(); i++) {
            String letra =  String.valueOf(cText.charAt(i));
            if (!CRYPTOGRAM.containsKey(letra)) {
                dText += letra;
            } else {
                dText += CRYPTOGRAM.get(letra);
            }
        }
        return dText;
    }
    
    
    /*A1Z26 Cipher*/
    public static String A1Z26(String cifrado) {
        
        String[] CRYPTOGRAM = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
        
        String[] palabras = cifrado.split(" |'|’|\"");
        String[] signos = cifrado.replaceAll("-", "").split("\\d+");
        int count = 1;
        String descifrado = "";
        for (String palabra: palabras) {
            String[] letras = palabra.split("\\D+");
            for (String letra: letras) {
                if (!letra.equals("")) {
                    int index = Integer.parseInt(letra)-1;
                    descifrado += CRYPTOGRAM[index];
                } else {
                    count--;
                    descifrado = descifrado.substring(0, descifrado.lastIndexOf(signos[count])-1);
                }
            }
            if (count < signos.length) {
                descifrado += signos[count];
                count++;
            }
        }
        return descifrado;
    }
    
    /*Combined cipher*/
    public static String combinedCipher(String cifrado) {
        String[] CRYPTOGRAM = {"W","V","U","T","S","R","Q","P","O","N","M","L","K","J","I","H","G","F","E","D","C","B","A","Z","Y","X"};
        
        String[] palabras = cifrado.split(" |'|’|\"");
        String[] signos = cifrado.replaceAll("-", "").split("\\d+");
        int count = 1;
        String descifrado = "";
        for (String palabra: palabras) {
            String[] letras = palabra.split("\\D+");
            for (String letra: letras) {
                if (!letra.equals("")) {
                    int index = Integer.parseInt(letra)-1;
                    descifrado += CRYPTOGRAM[index];
                } else {
                    count--;
                    descifrado = descifrado.substring(0, descifrado.lastIndexOf(signos[count])-1);
                }
            }
            if (count < signos.length) {
                descifrado += signos[count];
                count++;
            }
        }
        return descifrado;
    }
    
    /*Vigenere*/
    public static String vigenere(String texto, String clave) {
        String descifrado = "";
        texto = texto.toUpperCase();
        for (int i = 0, j = 0; i < texto.length(); i++) {
            char c = texto.charAt(i);
           
            if(!Character.isLetter(c)){
                descifrado = descifrado + String.valueOf(c);
            }
            
            if (c < 'A' || c > 'Z') continue;
            descifrado += (char)((c - clave.charAt(j) + 26) % 26 + 'A');
            j = ++j % clave.length();
                        
        }
        return descifrado;
    }
    
    /*Binario*/
    public static String binary(String cText){
        String dText="";
        String temp = "";
        for(int i=0;i<cText.length();i++){
            char c = cText.charAt(i);
                if (c == '0' || c == '1') {
                    temp += c;
                    if(temp.length() == 8){
                        char letra =(char)Integer.parseInt(temp,2);
                        dText += letra;
                        temp="";
                    }
                }
        }
        return dText.toUpperCase();
    }
    
    /*Shorts - Numeric code*/
    public static String shortsNumericCode(String texto){
        
        /*El codigo cifrado por episodio, los cuales son necesarios para el numeric code*/
        Map<Integer, String> mapa = new HashMap<>();
        mapa.put(1, "WELCOME TO GRAVITY FALLS");
        mapa.put(2, "NEXT WEEK: RETURN TO BUTT ISLAND");
        mapa.put(3, "HE'S STILL IN THE VENTS");
        mapa.put(5, "ONWARDS AOSHIMA!");
        mapa.put(6, "MR. CAESARIAN WILL BE OUT NEXT WEEK. MR. ATBASH WILL SUBSTITUTE");
        mapa.put(9, "NOT H.G. WELLS APPROVED.");
        mapa.put(10, "SORRY, DIPPER, BUT YOUR WENDY IS IN ANOTHER CASTLE.");
        mapa.put(11, "THE INVISIBLE WIZARD IS WATCHING.");
        mapa.put(12, "BROUGHT TO YOU BY HOMEWORK: THE CANDY.");
        mapa.put(13, "HEAVY IS THE HEAD THAT WEARS THE FEZ.");
        mapa.put(14, "NEXT UP: FOOTBOT TWO: GRUNKLE'S GREVENGE.");
        mapa.put(15, "VIVAN LOS PATOS DE LA PISCINA.");
        mapa.put(16, "BUT WHO STOLE THE CAPERS?");
        mapa.put(17, "HAPPY NOW, ARIEL?");
        mapa.put(20, "SEARCH FOR THE BLINDEYE");        
        
        String numero = ""; /*Numero del episodio en String*/
        String indice = ""; /*Indice del episodio en String*/
        
        int numeroInt = 0; /*Numero del episodio en entero*/
        int indiceInt = 0; /*Indice del episodio en entero*/
        
        //String cadena = ""; /*Cadena sobre el nombre del episodio*/
        String respuesta = "";
        
        for (int i = 0; i < texto.length(); i++) {            
                       
            if(Character.isDigit(texto.charAt(i))){ 
                numero = numero + texto.charAt(i); 
                indice = indice + texto.charAt(i);
            }            
            
            if(String.valueOf(texto.charAt(i)).equals(",") || String.valueOf(texto.charAt(i)).equals(" ") || String.valueOf(texto.charAt(i)).equals("[")){
                numero = "";
            } else if (String.valueOf(texto.charAt(i)).equals(")")) { 
                numeroInt = Integer.parseInt(numero);
                numero = "";                
            } 
            
            if(String.valueOf(texto.charAt(i)).equals(")") || String.valueOf(texto.charAt(i)).equals("[")){
                indice = "";
            } else if (String.valueOf(texto.charAt(i)).equals(",") || String.valueOf(texto.charAt(i)).equals(" ") || String.valueOf(texto.charAt(i)).equals("]")) { 
                indiceInt = Integer.parseInt(indice); 
                String r = mapa.get(numeroInt);
                
                /*Recordar que los caracteres especiales no cuentan, por eso el replace*/
                r = r.replace(" ","");
                r = r.replace(",","");
                r = r.replace(":","");
                r = r.replace("'","");
                r = r.replace(".","");
                
                respuesta = respuesta + String.valueOf(r.charAt(indiceInt - 1));
                                                    
                indice = "";
            }            
            
            /*Cuando exista un ] querra decir que una palabra termina y es necesario un espacio*/
            if(String.valueOf(texto.charAt(i)).equals("]")){
                respuesta = respuesta + " ";
            }
            
        }
        
        return respuesta;
    }
    
}
